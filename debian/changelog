jupyter-sphinx-theme (0.0.6+ds1-12) unstable; urgency=medium

  * Team upload.
  * Remove dependency on deprecated python3-entrypoints

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update renamed lintian tag names in lintian overrides.
  * Update standards version to 4.6.2, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Fri, 13 Sep 2024 13:26:25 +0200

jupyter-sphinx-theme (0.0.6+ds1-11) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + jupyter-sphinx-theme-doc: Add Multi-Arch: foreign.

  [ Jerome Benoit ]
  * d/patches/debianization.patch: setup.py, set use_2to3 to False
    (closes: #997435).
  * d/copyright: referesh copyright year-tuples.
  * d/copyright: update Source.
  * d/control: bump Standards-Version to 4.6.1 (no change).
  * d/jupyter-sphinx-theme{-doc,-common}.lintian-overrides: discard.
  * d/control: introduce Rules-Requires-Root and set it to no.
  * d/tests/control: add tex-gyre to Depends list.
  * d/control, jupyter-sphinx-theme-doc Package: add tex-gyre to
    Recommends list.
  * d/adhoc/examples/Makefile: refresh header.
  * d/adhoc/examples/samples/helloworld.py: migrate to Python 3.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 29 May 2022 15:15:35 +0000

jupyter-sphinx-theme (0.0.6+ds1-10) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Dmitry Shachnev ]
  * Update Homepage field.
  * Make the autopkgtest pass with Sphinx 2.4 (closes: #958661):
    - Remove htmlhelp testing from examples Makefile, sphinxcontrib-htmlhelp
      is not yet packaged in Debian.
    - Add dependencies on sphinxcontrib-{devhelp,qthelp,serializinghtml}.
  * Bump Standards-Version to 4.5.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 28 Apr 2020 11:47:47 +0300

jupyter-sphinx-theme (0.0.6+ds1-9) unstable; urgency=medium

  * RC fix release (Closes: #941639), update dependencies in autopkg test.
  * Debianization:
    - debian/control:
      - Standards Version, bump to 4.4.1 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 04 Oct 2019 20:35:50 +0000

jupyter-sphinx-theme (0.0.6+ds1-8) unstable; urgency=medium

  [ Jerome Benoit ]
  * Update TeX dependencies (Closes: #940896).
  * Bump debhelper to 12.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 26 Sep 2019 18:00:38 +0000

jupyter-sphinx-theme (0.0.6+ds1-7) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump Standards-Version to 4.4.0.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 26 Sep 2019 17:49:01 +0000

jupyter-sphinx-theme (0.0.6+ds1-6) unstable; urgency=medium

  * Serious fix release, harden reintroduction for Python 2 support
    (Closes: #920705).
  * Debianization:
    - debian/rules:
      - override_dh_installdocs target, reintroduce support for Python 2;
    - debian/python-jupyter-sphinx-theme.maintscript, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 01 Feb 2019 05:22:58 +0000

jupyter-sphinx-theme (0.0.6+ds1-5) unstable; urgency=medium

  * Serious bug fix release, (Closes: #919836).
  * Debianization:
    - debian/{copyright,adhoc/examples/Makefile}:
      - Copyright year tuple, update;
    - debian/jupyter-sphinx-theme-common.links, migration to Python 3;
    - debian/{control,rules}, reintroduce Python 2 support;
    - debian/adhoc/examples/Makefile, header, revisit.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 24 Jan 2019 15:51:01 +0000

jupyter-sphinx-theme (0.0.6+ds1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Jerome Benoit ]
  * d/{control,rules}: Remove Python 2 support
  * d/control: Bumb Standards-Version to 4.3.0 (no change)
  * d/rules: Discard get-orig-source target
  * d/t/control: Refrech

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 28 Dec 2018 09:19:41 +0000

jupyter-sphinx-theme (0.0.6+ds1-3) unstable; urgency=medium

  * Debianization:
    - debian/copyright:
      - copyright year tuple, update;
      - refresh;
    - debian/control:
      - debhelper, bump to 11;
      - Standards Version, bump to 4.1.3 (no change);
      - Build-Depends field, remove (was empty);
      - Recommends, jupyter-sphinx-theme-doc, refresh;
    - debian/rules:
      - debhelper, bump to 11;
      - get-orig-source target, compression option, move to d/watch;
    - debian/tests/*:
      - Restrictions field, add allow-stderr;
    - debian/adhoc/examples/samples/*:
      - d/a/e/s/helloworld.py , introduce;
    - debian/adhoc/examples/demo/*:
      - d/a/e/d/source/_sources/examples.txt
        link to ../examples.rst , introduce;
      - d/a/e/d/source/_themes/bootstrap , remove (Closes: #861505);
      - d/a/e/d/conf.py , harden;
      - d/a/e/d/examples.rst , local links, refresh;
    - debian/watch, harden;
    - debian/source/lintian-overrides, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 03 Feb 2018 14:11:58 +0000

jupyter-sphinx-theme (0.0.6+ds1-2) unstable; urgency=medium

  * Team upload.
  * Don't add an extra symlink to jquery; sphinx already does it.

 -- Ximin Luo <infinity0@debian.org>  Mon, 05 Dec 2016 17:26:27 +0100

jupyter-sphinx-theme (0.0.6+ds1-1) unstable; urgency=medium

  * Initial release (Closes: #838798).

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 27 Nov 2016 01:27:38 +0000
