Source: jupyter-sphinx-theme
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
Build-Depends-Indep: dh-python,
                     libjs-bootstrap,
                     libjs-bootswatch,
                     libjs-jquery,
                     python3-all,
                     python3-nbsphinx,
                     python3-recommonmark,
                     python3-setuptools,
                     python3-sphinx-bootstrap-theme
Standards-Version: 4.6.2
Homepage: https://github.com/jupyter/jupyter-sphinx-theme
Vcs-Git: https://salsa.debian.org/python-team/packages/jupyter-sphinx-theme.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/jupyter-sphinx-theme

Package: python3-jupyter-sphinx-theme
Architecture: all
Depends: jupyter-sphinx-theme-common (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends}
Suggests: jupyter-sphinx-theme-doc (= ${source:Version})
Description: Jupyter Sphinx Theme -- Python 3
 A Jupyter Sphinx theme for narrative documentation.
 .
 It integrates the Bootstrap CSS / JavaScript framework with various layout
 options, hierarchical menu navigation, and mobile-friendly responsive design.
 It is configurable, extensible and can use any number of different Bootswatch
 CSS themes.
 .
 This package contains the Python 3 version of the theme.

Package: jupyter-sphinx-theme-common
Architecture: all
Depends: libjs-bootstrap,
         libjs-bootswatch,
         libjs-jquery,
         python3-sphinx-bootstrap-theme,
         ${misc:Depends},
         ${python3:Depends}
Description: Jupyter Sphinx Theme -- common files
 A Jupyter Sphinx theme for narrative documentation.
 .
 It integrates the Bootstrap CSS / JavaScript framework with various layout
 options, hierarchical menu navigation, and mobile-friendly responsive design.
 It is configurable, extensible and can use any number of different Bootswatch
 CSS themes.
 .
 This package contains the common files used by the theme.

Package: jupyter-sphinx-theme-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends:
 python3-doc,
 python3-jupyter-sphinx-theme (= ${binary:Version}),
 texinfo,
 tex-gyre,
 texlive-fonts-recommended,
 texlive-plain-generic,
 texlive-latex-extra,
 texlive-latex-recommended,
 latexmk
Multi-Arch: foreign
Description: Jupyter Sphinx Theme -- documentation
 A Jupyter Sphinx theme for narrative documentation.
 .
 It integrates the Bootstrap CSS / JavaScript framework with various layout
 options, hierarchical menu navigation, and mobile-friendly responsive design.
 It is configurable, extensible and can use any number of different Bootswatch
 CSS themes.
 .
 This package contains the common documentation and examples.
